#include "local_planner_manager/local_planner_manager_node.hpp"
#include "local_planner_manager/local_planner_manager_state.hpp"
namespace roar
{
  namespace planning
  {
    namespace local
    {
      LocalPlannerManagerNode::LocalPlannerManagerNode() : LifecycleNode("manager", "local_planner", true), m_plugin_loader_("local_planner_manager", "roar::planning::local::LocalPlannerPlugin")
      {
        m_config_ = std::make_shared<LocalPlannerManagerConfig>(
            LocalPlannerManagerConfig{
                declare_parameter<bool>("manager.debug", false),
                declare_parameter<double>("manager.loop_rate", 20.0),
                declare_parameter<bool>("manager.should_loop_on_finish", true),
            });

        if (m_config_->debug)
        {
          bool _ = rcutils_logging_set_logger_level(get_logger().get_name(),
                                                    RCUTILS_LOG_SEVERITY_DEBUG); // enable or disable debug
        }



        RCLCPP_INFO(this->get_logger(), "LocalPlannerManagerNode initialized with Debug Mode = [%s]",
                    m_config_->debug ? "YES" : "NO");

        RCLCPP_INFO(this->get_logger(), "LocalPlanner is Looping on Finish = [%s]",
                    m_config_->should_loop_on_finish ? "YES" : "NO");

        // initialize plugins
        const auto plugin_names = declare_parameter("plugins", std::vector<std::string>{});
        RCLCPP_INFO_STREAM(this->get_logger(), "plugin_names: " << plugin_names.size() << " plugins");
        
        if (plugin_names.size() == 0)
        {
          RCLCPP_ERROR(this->get_logger(), "No plugins found");
          return;
        }

        for (const auto &plugin_name : plugin_names)
        {
          LocalPlannerPlugin::SharedPtr new_plugin = m_plugin_loader_.createSharedInstance(plugin_name);
          m_plugins_.push_back(new_plugin);
          RCLCPP_DEBUG_STREAM(this->get_logger(), "plugin: " << plugin_name << " loaded");
        }
        std::for_each(
            m_plugins_.begin(), m_plugins_.end(), [this](LocalPlannerPlugin::SharedPtr &p)
            { 
                p->initialize(this);
                RCLCPP_DEBUG_STREAM(this->get_logger(), "plugin: " << p->get_plugin_name() << " initialized"); });

        m_state_ = std::make_shared<LocalPlannerManagerState>();
      }
      LocalPlannerManagerNode ::~LocalPlannerManagerNode()
      {
      }

      /**
       * lifecycle
       */
      nav2_util::CallbackReturn LocalPlannerManagerNode::on_configure(const rclcpp_lifecycle::State &state)
      {
        RCLCPP_DEBUG(this->get_logger(), "on_configure");

        this->odom_sub_ = this->create_subscription<nav_msgs::msg::Odometry>(
            "/odometry", rclcpp::SystemDefaultsQoS(),
            std::bind(&LocalPlannerManagerNode::onLatestOdomReceived, this, std::placeholders::_1));

        this->footprint_sub_ = this->create_subscription<geometry_msgs::msg::PolygonStamped>(
            "/footprint", rclcpp::SystemDefaultsQoS(),
            std::bind(&LocalPlannerManagerNode::onLatestFootprintReceived, this, std::placeholders::_1));
        this->occupancy_map_sub_ = this->create_subscription<nav_msgs::msg::OccupancyGrid>(
            "/occupancy_map", rclcpp::SystemDefaultsQoS(),
            std::bind(&LocalPlannerManagerNode::onLatestOccupancyMapReceived, this, std::placeholders::_1));

        this->global_plan_sub_ = this->create_subscription<nav_msgs::msg::Path>(
            "/global_path", rclcpp::SystemDefaultsQoS(),
            std::bind(&LocalPlannerManagerNode::onLatestGlobalPlanReceived, this, std::placeholders::_1));

        this->best_path_vis_marker_pub_ = this->create_publisher<visualization_msgs::msg::MarkerArray>(
            "/best_path_vis", rclcpp::SystemDefaultsQoS());

        this->best_path_pub_ = this->create_publisher<nav_msgs::msg::Path>(
            "/best_path", rclcpp::SystemDefaultsQoS());

        // diagnostic server
        diagnostic_pub_ = this->create_publisher<diagnostic_msgs::msg::DiagnosticArray>(
            "/diagnostics", 10);

        // tf buffer
        tf_buffer_ =
            std::make_shared<tf2_ros::Buffer>(this->get_clock());
        tf_listener_ =
            std::make_shared<tf2_ros::TransformListener>(*tf_buffer_);
        std::for_each(
            m_plugins_.begin(), m_plugins_.end(), [this](LocalPlannerPlugin::SharedPtr &p)
            { 
                p->configure(m_config_);
                p->setTfBuffer(tf_buffer_);
                RCLCPP_DEBUG_STREAM(this->get_logger(), "plugin: " << p->get_plugin_name() << " configured"); });
        return nav2_util::CallbackReturn::SUCCESS;
      }
      nav2_util::CallbackReturn LocalPlannerManagerNode::on_activate(const rclcpp_lifecycle::State &state)
      {
        RCLCPP_DEBUG(this->get_logger(), "on_activate");
        int loop_rate_milliseconds = int(this->m_config_->loop_rate * 1000);

        RCLCPP_DEBUG(this->get_logger(), "loop_rate: %.3f", m_config_->loop_rate);
        this->execution_timer = this->create_wall_timer(
            std::chrono::milliseconds(int(loop_rate_milliseconds)),
            std::bind(&LocalPlannerManagerNode::execute, this));
        this->diagnostic_pub_->on_activate();
        this->best_path_vis_marker_pub_->on_activate();
        this->best_path_pub_->on_activate();
        return nav2_util::CallbackReturn::SUCCESS;
      }
      nav2_util::CallbackReturn LocalPlannerManagerNode::on_deactivate(const rclcpp_lifecycle::State &state)
      {
        RCLCPP_DEBUG(this->get_logger(), "on_deactivate");
        execute_timer->cancel();
        diagnostic_pub_->on_deactivate();
        best_path_vis_marker_pub_->on_deactivate();
        best_path_pub_->on_deactivate();
        return nav2_util::CallbackReturn::SUCCESS;
      }
      nav2_util::CallbackReturn LocalPlannerManagerNode::on_cleanup(const rclcpp_lifecycle::State &state)
      {
        RCLCPP_DEBUG(this->get_logger(), "on_cleanup");
        this->odom_sub_ = nullptr;

        return nav2_util::CallbackReturn::SUCCESS;
      }
      nav2_util::CallbackReturn LocalPlannerManagerNode::on_shutdown(const rclcpp_lifecycle::State &state)
      {
        RCLCPP_DEBUG(this->get_logger(), "on_shutdown");
        return nav2_util::CallbackReturn::SUCCESS;
      }

      /**
       * Controller interaction
       */
      void LocalPlannerManagerNode::control_send_goal(const nav_msgs::msg::Path::SharedPtr path)
      {
        RCLCPP_DEBUG_STREAM(this->get_logger(), "sending goal to controller");
        this->best_path_pub_->publish(*path);
      }
      

      bool LocalPlannerManagerNode::canExecute()
      {
        if (m_plugins_.size() == 0)
        {
          RCLCPP_ERROR_STREAM(this->get_logger(), "not executing: "
                                                      << "m_plugins_.size() == 0");
          return false;
        }
        if (this->didReceiveAllMessages() == false)
        {
          RCLCPP_DEBUG_STREAM(this->get_logger(), "not executing: "
                                                      << "didReceiveAllMessages() == false");
          return false;
        }
        if (this->num_execution >= 1) {
          RCLCPP_DEBUG_STREAM(this->get_logger(), "not executing: "
                                                      << "num_execution >= 1");
          return false;
        }
        return true;
      }
      bool LocalPlannerManagerNode::didReceiveAllMessages()
      {
        bool isOK = true;
        if (this->m_state_->odom == nullptr)
        {
          RCLCPP_DEBUG(this->get_logger(), "odom not received, not executing...");
          isOK = false;
        }
        // if (this->m_state_->robot_footprint == nullptr)
        // {
        //   RCLCPP_DEBUG(this->get_logger(), "latest_footprint_ not received, not executing...");
        //   isOK = false;
        // }
        if (this->m_state_->global_plan == nullptr)
        {
          RCLCPP_DEBUG(this->get_logger(), "global_plan_ not received, not executing...");
          isOK = false;
        }
        // if (this->m_state_->occupancy_map == nullptr)
        // {
        //   RCLCPP_DEBUG(this->get_logger(), "occupancy_map_ not received, not executing...");
        //   isOK = false;
        // }
        return isOK;
      }

      /**
       * helper methods
       */
      void LocalPlannerManagerNode::execute()
      {
        RCLCPP_DEBUG(this->get_logger(), "-----LocalPlannerManagerNode START -----");
        if (this->canExecute()) // only one request at a time
        {
          // prevent concurrent execution
          this->num_execution += 1;

          // update every planner plugin
          std::for_each(
              m_plugins_.begin(), m_plugins_.end(), [this](LocalPlannerPlugin::SharedPtr &p)
              { 
                  p->update(this->m_state_);
                  RCLCPP_DEBUG_STREAM(this->get_logger(), "plugin: " << p->get_plugin_name() << " updated"); 
              }
          );

          // compute every planner plugin
          std::map<std::string, nav_msgs::msg::Path::SharedPtr> pathsMap;
          std::for_each(
              m_plugins_.begin(), m_plugins_.end(), [this, &pathsMap](LocalPlannerPlugin::SharedPtr &p)
              { 
                  nav_msgs::msg::Path::SharedPtr path = p->compute();
                  if (path != nullptr) {
                    pathsMap[p->get_plugin_name()] = path;
                    RCLCPP_DEBUG_STREAM(this->get_logger(), "plugin: " << p->get_plugin_name() << " computed path of length: " << path->poses.size());
                  } else {
                    RCLCPP_DEBUG_STREAM(this->get_logger(), "plugin: " << p->get_plugin_name() << " returned null path");
                  }
              }
          );

          // pick path
          std::string best_path_key = this->findBestPath(pathsMap);
          nav_msgs::msg::Path::SharedPtr best_path = pathsMap[best_path_key];
          if (best_path == nullptr) {
              // RCLCPP_WARN_STREAM(this->get_logger(), "no path selected");
              this->num_execution -= 1;
              return;
          } else {
              RCLCPP_DEBUG_STREAM(this->get_logger(), "selected path from [" << best_path_key << "] of length: " << best_path->poses.size());
          }

          // publish path
          this->publishBestPathVisualizationMarker(best_path);

          // send path to controller
          this->control_send_goal(best_path);
          this->num_execution -= 1;
        }
        RCLCPP_DEBUG(this->get_logger(), "");
      }

      std::string LocalPlannerManagerNode::findBestPath(const std::map<std::string, nav_msgs::msg::Path::SharedPtr> pathsMap)
      {
        std::string best_path_key = "";
        int best_path_length = 0;
        for (auto const &pair : pathsMap)
        {
          std::string key = pair.first;
          nav_msgs::msg::Path::SharedPtr path = pair.second;
          if (path != nullptr && path->poses.size() > best_path_length)
          {
            best_path_key = key;
            best_path_length = path->poses.size();
          }
        }
        return best_path_key;
      }
      
    } // local
  }   // planning
} // roar
int main(int argc, char **argv)
{
  rclcpp::init(argc, argv);
  auto node = std::make_shared<roar::planning::local::LocalPlannerManagerNode>();

  rclcpp::spin(node->get_node_base_interface());
  rclcpp::shutdown();

  return 0;
}