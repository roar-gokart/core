#ifndef ROAR_PLANNING__PLUGIN__DUMMY_PLANNER_PLUGIN_CPP_
#define ROAR_PLANNING__PLUGIN__DUMMY_PLANNER_PLUGIN_CPP_

#include "rclcpp/rclcpp.hpp"
#include "local_planner_manager/local_planner_manager_state.hpp"
#include "local_planner_manager/local_planner_plugin_interface.hpp"
#include "nav_msgs/msg/path.hpp"
#include "nav_msgs/msg/odometry.hpp"
#include "geometry_msgs/msg/pose_stamped.hpp"
#include <vector>
using namespace std::chrono_literals;

namespace roar
{
    namespace planning
    {
        namespace local
        {

            class SimpleDistancePlanner : public LocalPlannerPlugin
            {
                
            public:
                struct State
                {
                    nav_msgs::msg::Odometry::SharedPtr latest_odom;
                    nav_msgs::msg::Path::SharedPtr global_plan_;
                };

                struct Config
                {
                    double next_waypoint_dist;
                    bool should_loop_on_finish = true;
                    bool debug = false;
                };

                SimpleDistancePlanner() = default;
                ~SimpleDistancePlanner() = default;

                void initialize(nav2_util::LifecycleNode *node) override
                {
                    LocalPlannerPlugin::initialize(node);

                    RCLCPP_DEBUG(logger_, "Initializing SimpleDistancePlanner");
                    node_ = node;
                    this->m_state_ = std::make_shared<State>();
                    this->config_ = Config{
                        node_->declare_parameter<double>("SimpleDistancePlanner.next_waypoint_dist", 5.0),
                        node_->declare_parameter<bool>("SimpleDistancePlanner.should_loop_on_finish", true),
                        node_->declare_parameter<bool>("SimpleDistancePlanner.debug", true),

                    };
                    RCLCPP_DEBUG_STREAM(logger_, "next_waypoint_dist: " << this->config_.next_waypoint_dist);
                              if (this->config_.debug)
          {
                    RCLCPP_DEBUG_STREAM(logger_, "Setting debug level for "
                                                    << this->get_plugin_name() << " to DEBUG");
                    bool _ = rcutils_logging_set_logger_level(this->get_plugin_name(),
                                                            RCUTILS_LOG_SEVERITY_DEBUG);
          }

                }

                const char *get_plugin_name() override
                {
                    return "SimpleDistancePlanner";
                }

                bool configure(const LocalPlannerManagerConfig::SharedPtr config) override
                {
                    RCLCPP_DEBUG(logger_, "Configuring SimpleDistancePlanner");
                    this->config_.should_loop_on_finish = config->should_loop_on_finish;
                    return true;
                }

                bool update(const LocalPlannerManagerState::SharedPtr state) override
                {
                    RCLCPP_DEBUG(logger_, "Updating SimpleDistancePlanner");
                    this->m_state_->latest_odom = state->odom;
                    this->m_state_->global_plan_ = state->global_plan;
                    return true;
                }

                nav_msgs::msg::Path::SharedPtr compute() override
                {
                    RCLCPP_DEBUG(logger_, "Computing SimpleDistancePlanner");
                    if (this->m_state_->latest_odom == nullptr)
                    {
                        RCLCPP_DEBUG_STREAM(logger_, "latest_odom is null, not computing");
                        return nullptr;
                    }
                    if (this->m_state_->global_plan_ == nullptr)
                    {
                        RCLCPP_DEBUG_STREAM(logger_, "global_plan_ is null, not computing");
                        return nullptr;
                    }

                    nav_msgs::msg::Path::SharedPtr path = this->findPathToNextWaypoint(this->config_.next_waypoint_dist, *this->m_state_->latest_odom, *this->m_state_->global_plan_);
                    if (path == nullptr || path->poses.size() == 0)
                    {
                        RCLCPP_DEBUG_STREAM(logger_, "findPathToNextWaypoint yielded is null, unable to find path");
                        return nullptr;
                    }

                    // RCLCPP_DEBUG_STREAM(logger_, "path: " << path->poses.size() << " poses[-1]: " << path->poses.back().pose.position.x << ", " << path->poses.back().pose.position.y);

                    return path;
                }

                nav_msgs::msg::Path::SharedPtr findPathToNextWaypoint(const float next_waypoint_dist, nav_msgs::msg::Odometry odom, nav_msgs::msg::Path global_plan)
                {
                    size_t closest_waypoint_index = this->pFindClosestWaypointIndex(odom, global_plan);
                    size_t next_waypoint_index = this->findNextWaypointIndex(closest_waypoint_index, next_waypoint_dist, odom, global_plan);
                    
                    // get waypoints from closest waypoint index to next_waypoint index
                    std::vector<geometry_msgs::msg::PoseStamped> waypoints;

                    if (closest_waypoint_index > next_waypoint_index) {
                        // when we loop around
                        waypoints.push_back(global_plan.poses[next_waypoint_index]);
                    } else {
                        // when we don't loop around, on the track, closest waypoint should be ALWAYS < next_waypoint
                        for (size_t i = closest_waypoint_index; i <= next_waypoint_index; i++)
                        {
                            waypoints.push_back(global_plan.poses[i]);
                        }
                    }
                    
                    RCLCPP_DEBUG_STREAM(logger_, "[SimpleDistancePlanner] waypoints: " << waypoints.size() << " poses. last pose:: " << waypoints.back().pose.position.x << ", " << waypoints.back().pose.position.y);

                    nav_msgs::msg::Path::SharedPtr path = std::make_shared<nav_msgs::msg::Path>();
                    path->header = global_plan.header;
                    path->header.stamp = node_->now();
                    path->poses = waypoints;

                    RCLCPP_DEBUG_STREAM(logger_, "[SimpleDistancePlanner] path: [" << path->poses.size() << "] poses. last pose:: " << path->poses.back().pose.position.x << ", " << path->poses.back().pose.position.y);

                    return path;
                }
                
                size_t pFindClosestWaypointIndex(nav_msgs::msg::Odometry odom, nav_msgs::msg::Path global_plan)
                {
                    double min_distance = std::numeric_limits<double>::max();
                    size_t closest_waypoint_index = 0;
                    for (size_t i = 0; i < global_plan.poses.size(); i++)
                    {
                        double distance = std::sqrt(std::pow(odom.pose.pose.position.x - global_plan.poses[i].pose.position.x, 2) +
                                                    std::pow(odom.pose.pose.position.y - global_plan.poses[i].pose.position.y, 2));
                        if (distance < min_distance)
                        {
                            min_distance = distance;
                            closest_waypoint_index = i;
                        }
                    }
                    return closest_waypoint_index;
                }

                size_t findNextWaypointIndex(const size_t closest_waypoint_index, const float next_waypoint_min_dist, nav_msgs::msg::Odometry odom, nav_msgs::msg::Path global_plan)
                {
                    RCLCPP_DEBUG_STREAM(logger_, "[SimpleDistancePlanner] closest_waypoint_index:" << closest_waypoint_index);
                    RCLCPP_DEBUG(logger_, "[SimpleDistancePlanner] finding waypoint");
                    // find the next waypoint, including looping back to the beginning, if needed
                    // double next_waypoint_dist = cte_and_lookahead.second;
                    double next_waypoint_dist = next_waypoint_min_dist;
                    size_t next_waypoint_index = (closest_waypoint_index+1)%global_plan.poses.size();
                    for (size_t i = 0; i < global_plan.poses.size(); i++)
                    {
                        size_t next_index = (closest_waypoint_index + i) % global_plan.poses.size();

                        if (!this->config_.should_loop_on_finish) {
                            next_index = std::min(closest_waypoint_index+i, global_plan.poses.size() - 1);
                        }
                        double distance = std::sqrt(std::pow(odom.pose.pose.position.x - global_plan.poses[next_index].pose.position.x, 2) +
                                                    std::pow(odom.pose.pose.position.y - global_plan.poses[next_index].pose.position.y, 2));
                        if (distance > next_waypoint_dist)
                        {
                            next_waypoint_index = next_index;
                            break;
                        }
                    }
                    RCLCPP_DEBUG_STREAM(logger_, "[SimpleDistancePlanner] next waypoint index: " << next_waypoint_index << ", next_waypoint_dist: " << next_waypoint_dist);
                    return next_waypoint_index;
                }

            private:
                nav2_util::LifecycleNode *node_{};
                std::shared_ptr<State> m_state_;
                Config config_;
            };
        } // namespace local
    }     // namespace planning
} // namespace roar

#include "pluginlib/class_list_macros.hpp"
PLUGINLIB_EXPORT_CLASS(roar::planning::local::SimpleDistancePlanner, roar::planning::local::LocalPlannerPlugin)

#endif // ROAR_PLANNING__PLUGIN__DUMMY_PLANNER_PLUGIN_CPP_
